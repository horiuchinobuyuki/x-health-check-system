<?php
/*
The MIT License (MIT)

Copyright (c) 2013 xsharing

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

require_once('model/page.php');
require_once('model/result.php');
require_once('config/settings.php');

$pages = XHCConfig::$pages;
$results = array();

foreach ($pages as $page) {
	$url = parse_url($page->url);
	
	$host_name = $url['host'];
	$hosts = dns_get_record($host_name, DNS_A);
	
	foreach($hosts as $host) {
		$result = new Result();
		$host = $host['ip'];
		
		$result->url = $page->url;
		$result->host = $host;
		
		$url['host'] = $host;
		$special_url = http_build_url($url);
		
		$mt = microtime(true);
		$ret = exec("wget --spider --server-response --timeout 1 --tries 3 --header 'Host: {$host_name}' {$special_url} >/dev/null 2>&1; echo $?");
		
		$result->time = microtime(true) - $mt;
		$result->success = ($ret == '0') ? true : false;
		
		$results[] = $result;
	}
}


print_r(json_encode($results, JSON_NUMERIC_CHECK|JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE|JSON_PRETTY_PRINT));